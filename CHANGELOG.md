# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.1.14](http://gitlab.com/wcskj/integracja-open-nexus/compare/v0.1.13...v0.1.14) (2021-04-21)


### Bug Fixes

* Fetching attachments. Dockerfile dependencies bump. ([553bc86](http://gitlab.com/wcskj/integracja-open-nexus/commit/553bc868a9bb3e5f50bee8bebf11e34c46ef6cc9))

### [0.1.13](http://gitlab.com/wcskj/integracja-open-nexus/compare/v0.1.12...v0.1.13) (2021-03-27)


### Bug Fixes

* Number of attachments. ([4ea87cc](http://gitlab.com/wcskj/integracja-open-nexus/commit/4ea87cc937f5e5d036d38581be1368a2282c867e))

### [0.1.12](http://gitlab.com/wcskj/integracja-open-nexus/compare/v0.1.11...v0.1.12) (2021-02-12)


### Bug Fixes

* Numer of attachments. ([3cbb03d](http://gitlab.com/wcskj/integracja-open-nexus/commit/3cbb03de2106f1ea3e8c1feb970e6823be9aa12a))
* return proper next page link. ([57fcfac](http://gitlab.com/wcskj/integracja-open-nexus/commit/57fcfacbcbc3c9b9041282eda542086ec0a7c5e4))

### 0.1.11 (2020-11-21)


### Bug Fixes

* return proper next page link. ([8121a1a](http://gitlab.com/wcskj/integracja-open-nexus/commit/8121a1a78d19e0211b2f6e278d67d0127ef1579e))
