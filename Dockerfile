FROM wordpress:cli-2.2.0-php7.1

ENV WCSKJ_HOME=/home/wcskj
ENV AUCTION_OUTPUT_PATH=${WCSKJ_HOME}/out/auction-db

USER root
RUN apk update
RUN apk add npm=10.24.0-r0
RUN apk add nodejs=10.24.0-r0

RUN mkdir /home/wcskj
COPY ./src /home/wcskj/src
COPY ./package.json \
    ./package-lock.json \
    ./settings.js \
    ./index.js \
    /home/wcskj/

RUN mkdir -p ${AUCTION_OUTPUT_PATH}
RUN chown -R www-data:www-data /home/wcskj

WORKDIR ${WCSKJ_HOME}

USER www-data
RUN npm config set loglevel warn
RUN npm install
ENTRYPOINT [ "npm", "start" ]
