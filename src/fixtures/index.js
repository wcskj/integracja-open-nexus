const fs = require("fs");

export const fakeAuctions = fs.readFileSync(
  `${__dirname}/auctions.html`,
  "utf8"
);

export const fakeAuctionsClosedOnly = fs.readFileSync(
  `${__dirname}/auctions_closed_only.html`,
  "utf8"
);

export const fakeAuctionOpen = fs.readFileSync(
  `${__dirname}/auction_open.html`,
  "utf8"
);

export const fakeAuctionOpenModifiedAttachment = fs.readFileSync(
  `${__dirname}/auction_open_modified_attachment.html`,
  "utf8"
);

export const fakeAuctionOpenModifiedComment = fs.readFileSync(
  `${__dirname}/auction_open_modified_comment.html`,
  "utf8"
);

export const fakeAuctionOpenOver221k = fs.readFileSync(
  `${__dirname}/auction_open_over_221_k.html`,
  "utf8"
);

export const fakeAuctionClosed = fs.readFileSync(
  `${__dirname}/auction_closed.html`,
  "utf8"
);

export const fakeAuctions1stPage = fs.readFileSync(
  `${__dirname}/auctions_1st_page.html`,
  "utf8"
);

export const fakeAuctions2ndPage = fs.readFileSync(
  `${__dirname}/auctions_2nd_page.html`,
  "utf8"
);
