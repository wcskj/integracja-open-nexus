export const IntegrationType = {
  CREATE: "create",
  UPDATE: "update",
  STALE: "stale",
};
