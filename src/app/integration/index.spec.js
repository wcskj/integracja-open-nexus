import { expect } from "chai";
import { assert } from "sinon";
import { sandbox } from "../../test-setup.spec";
import * as auction from "../auction/index";
import * as integration from "../integration/index";
import { IntegrationType } from "./type";

describe("Integration module", () => {
  describe("saveAuction", () => {
    let createStub;
    beforeEach(() => {
      createStub = sandbox.stub(auction, "saveWordpressPost").rejects();
    });

    after(() => {
      createStub.restore();
    });

    it("should throw error if auction is not provided", async () => {
      const saveAuction = integration.saveAuction();
      try {
        await saveAuction(IntegrationType.STALE);
      } catch (e) {
        expect(e.message).to.be.eql("No Auction provided");
      }
    });

    it("should not create a post if the integration type is STALE", () => {
      integration.saveAuction(
        {
          id: "sampleId",
        },
        null
      )([IntegrationType.STALE, "fakeWordpressPostId"]);
      assert.notCalled(createStub);
    });

    it("should call a post creation method if the integration type is not STALE", () => {
      integration.saveAuction()(IntegrationType.CREATE);
      assert.calledOnce(createStub);
    });
  });

  describe("should resolve integration type", () => {
    const toCompare = {
      title: "someTitle",
    };

    const FAKE_AUCTION_ID = "fakeAuctionId";
    const FAKE_WORDPRESS_POST_ID = "fakePostId";

    it("to be STALE", async () => {
      const result = await integration.compareWith(
        { ...toCompare },
        FAKE_AUCTION_ID
      )({
        ...toCompare,
        postId: FAKE_WORDPRESS_POST_ID,
      });
      expect(result).to.eql([IntegrationType.STALE, FAKE_WORDPRESS_POST_ID]);
    });

    it("to be CREATE", async () => {
      const result = await integration.compareWith(
        { ...toCompare },
        FAKE_AUCTION_ID
      )(null);
      expect(result).to.eql([IntegrationType.CREATE, null]);
    });

    it("to be UDPATE", async () => {
      const result = await integration.compareWith(
        { ...toCompare },
        FAKE_AUCTION_ID
      )({
        ...toCompare,
        title: "someOtherTitle",
        postId: FAKE_WORDPRESS_POST_ID,
      });
      expect(result).to.eql([IntegrationType.UPDATE, FAKE_WORDPRESS_POST_ID]);
    });
  });
});
