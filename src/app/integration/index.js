import {
  saveWordpressPost,
  fetchAuctionContent,
  getIds,
  storeItem,
} from "../auction";
import { createDb } from "../db";
import { IntegrationType } from "./type";

export const compareWith = (fetchedAuction, auctionId) => dbAuction => {
  return dbAuction && dbAuction.postId
    ? [
        determineIntegrationType(fetchedAuction, dbAuction, auctionId),
        dbAuction.postId,
      ]
    : [IntegrationType.CREATE, null];
};

export const determineIntegrationType = (
  fetchedAuction,
  dbAuction,
  auctionId
) => {
  console.log(
    "LOG: Auction:",
    auctionId,
    `- Found auction ${auctionId} in the db, calculating diff...`
  );
  // prettier-ignore
  return [
    [fetchedAuction.title, dbAuction.title],
    [fetchedAuction.dates, dbAuction.dates],
    [fetchedAuction.requirements, dbAuction.requirements],
    [fetchedAuction.attachments, dbAuction.attachments],
    [fetchedAuction.comments, dbAuction.comments],
    [fetchedAuction.objectives, dbAuction.objectives],
  ].some(([propA, propB]) => !!propA && !!propB && propA !== propB)
    ? IntegrationType.UPDATE
    : IntegrationType.STALE;
};

export const saveAuction = (fetchedAuction, db) => async ([
  integrationType,
  postId,
]) => {
  if (integrationType !== IntegrationType.STALE) {
    if (integrationType === IntegrationType.UPDATE && !postId) {
      throw new Error("Trying to update the auction but postId is missing!");
    }
    await saveWordpressPost(integrationType, fetchedAuction, postId)
      .then(storeItem(db, integrationType))
      .catch(err => {
        return err;
      });
  } else if (fetchedAuction && integrationType == IntegrationType.STALE) {
    console.log(
      "LOG: Auction:",
      fetchedAuction.id,
      "- integration marked as stale, doing nothing."
    );
  } else {
    throw new Error("No Auction provided");
  }
  return integrationType;
};

export const integrateAuction = async (auctionId, db) => {
  const fetchedAuction = await fetchAuctionContent(auctionId);
  return db
    .get(auctionId)
    .then(compareWith(fetchedAuction, auctionId))
    .catch(_ => [IntegrationType.CREATE, null])
    .then(saveAuction(fetchedAuction, db));
};

export const runIntegration = url => {
  const db = createDb();
  return getIds(url).then(auctionIds =>
    auctionIds.reduce(async (previousPromise, nextAuctionId) => {
      await previousPromise;
      return integrateAuction(nextAuctionId, db);
    }, Promise.resolve())
  );
};
