import { expect } from "chai";
import { settings } from "../../../settings";
import { AuctionPriceRange } from "../model";
import { buildUrl } from "../utils";
import {
  getAttachments,
  getComments,
  getNextPageUrl,
  getObjectives,
  getPriceRangeCategory,
  getRequirements,
} from "./content";
const fetch = require("request-promise");
const $ = require("cheerio");

describe("AuctionContent module", () => {
  const PAST_AUCTION_ID = "242215";
  const AUCTION_OVER_221K_ID = "239434";
  const AUCTION_WITH_OBJECTIVES = "290629";

  let closedAuction;
  let auctionWithAttachments;
  let auctionWithObjectives;
  let over221kAuction;
  let auctionListFirstPage;

  before(async function() {
    this.timeout(15000);
    closedAuction = await fetch(buildUrl(PAST_AUCTION_ID));
    auctionWithAttachments = closedAuction;
    over221kAuction = await fetch(buildUrl(AUCTION_OVER_221K_ID));
    auctionListFirstPage = await fetch(settings.integration.urls.auction_list);
    auctionWithObjectives = await fetch(buildUrl(AUCTION_WITH_OBJECTIVES));
  });

  it("should not assign price category when price range keyword is missing", async () => {
    // GIVEN & WHEN
    const priceRange = await getPriceRangeCategory(closedAuction);
    // THEN
    expect(priceRange).to.be.undefined;
  });

  it("should assign price category when price range keyword is present", async () => {
    // GIVEN & WHEN
    const priceRange = await getPriceRangeCategory(over221kAuction);
    // THEN
    expect(priceRange).to.eql(AuctionPriceRange.OVER_221K.wordpressCategory);
  });

  it("should get the next auction page url", async () => {
    // GIVEN & WHEN
    const nextPageUrl = await getNextPageUrl(auctionListFirstPage);
    // THEN
    expect(nextPageUrl).not.to.be.undefined;
    expect(nextPageUrl).to.eql(
      `${settings.integration.urls.auction_list}?page=2`
    );
  });

  it("should not get the next auction page url", async () => {
    // GIVEN
    const auctionListLastPage = await fetch(
      getActionListLastPageUrl(auctionListFirstPage)
    );
    // WHEN
    const nextPageUrl = await getNextPageUrl(auctionListLastPage);
    // THEN
    expect(nextPageUrl).not.to.be.undefined;
    expect(nextPageUrl).to.be.false;

    function getActionListLastPageUrl(content) {
      const LAST_PAGE_SELECTOR = ".pagination li a";
      const LAST_PAGE_SIGN = "»";
      const paginationLinks = $(LAST_PAGE_SELECTOR, content).get();

      const lastPageSignIndex = paginationLinks.findIndex(
        link => $(link).text() === LAST_PAGE_SIGN
      );

      if (lastPageSignIndex !== -1) {
        const nextPageRelativeUrl = paginationLinks[lastPageSignIndex - 1];
        const relativeUrl = $(nextPageRelativeUrl)[0].attribs["href"];
        return `${settings.integration.urls.auction_list}${relativeUrl}`;
      }
      return false;
    }
  });

  it("should get the attachments", async () => {
    // GIVEN & WHEN
    const attachments = await getAttachments(auctionWithAttachments);
    // THEN
    const FILE_SELECTOR = "table tbody > tr";
    const fileElements = $(FILE_SELECTOR, attachments).get();
    const DOWNLOAD_LINK_COLUMN = 6;
    const fileUrls = fileElements.map(
      element =>
        $(`td:nth-child(${DOWNLOAD_LINK_COLUMN}) a`, element)[0].attribs["href"]
    );
    expect(fileUrls.length).to.eql(13);
    fileUrls.forEach(url => {
      expect(url.includes("//platformazakupowa.pl/file/")).to.be.true;
    });
  });

  it("should get auction objectives", async () => {
    // GIVEN & WHEN
    const objectives = await getObjectives(auctionWithObjectives);
    // THEN
    expect(objectives).not.to.be.undefined;
    expect(objectives).to.contain("Przedmiot");
    const tableHeaders = $("thead th", objectives).get();
    const EXPECTED_HEADERS = ["Lp", "Nazwa", "Opis i załączniki", "Ilość / Jm"];
    expect(tableHeaders.map(header => $(header).text())).to.eql(
      EXPECTED_HEADERS
    );
    const tableCells = $("tbody tr td", objectives).get();
    // expect 4 columns per row
    expect(tableCells.length % 4).to.eql(0);
    tableCells.forEach((cell, idx) => {
      if (idx === 0) {
        expect($(cell).text()).to.eql("1");
      }
      expect($(cell).text()).not.to.be.undefined;
    });
  });

  it("should get the comments section", async () => {
    // GIVEN & WHEN
    const comments = await getComments(auctionWithObjectives);
    // THEN
    expect(comments).not.to.be.undefined;
    expect(comments).to.contain("Komunikaty");
  });

  it("should get the requirements section", async () => {
    // GIVEN & WHEN
    const requirements = await getRequirements(auctionWithObjectives);
    // THEN
    expect(requirements).not.to.be.undefined;
    expect(requirements).to.contain("Wymagania i specyfikacja");
  });
});
