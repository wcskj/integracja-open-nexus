import { settings } from "../../../settings";
import { buildCommand, buildHTML, buildUrl } from "../utils";
import {
  getAttachments,
  getComments,
  getDatePosted,
  getDates,
  getNextPageUrl,
  getObjectives,
  getPriceRangeCategory,
  getRequirements,
  getTitle,
} from "./content";
import { promisify } from "util";
const exec = promisify(require("child_process").exec);

const fs = require("fs").promises;

const fetch = require("request-promise");
const $ = require("cheerio");

export const getIds = async url => {
  const html = await fetch(url);
  const auctionIds = $(
    ".table-responsive > table > tbody > tr > td:first-child",
    html
  );
  const idSelector = "a";
  const firstColumnValues = auctionIds
    .map((_, elem) => $(idSelector, elem).html())
    .get();
  const ids = firstColumnValues.filter(val => !Number.isNaN(parseInt(val)));
  const nextPageUrl = getNextPageUrl(html);
  if (nextPageUrl) {
    return ids.concat(await getIds(nextPageUrl));
  }
  return ids;
};

export const fetchAuctionContent = async auctionId => {
  const SELECTORS = {
    INFO: ".company-auction-section",
    REQUIREMENTS: "#requirements",
    COMMENTS: "#comments_info",
  };

  const url = buildUrl(auctionId);
  const content = await fetch(url);
  const info = $(SELECTORS.INFO, content);
  const title = getTitle(info);
  console.log("LOG: Auction:", auctionId, "- fetched info.");
  return {
    id: auctionId,
    title: title,
    posted: getDatePosted(info),
    dates: getDates(info),
    requirements: getRequirements(content),
    attachments: getAttachments(content),
    comments: getComments(content),
    priceRange: getPriceRangeCategory(title),
    objectives: getObjectives(content),
  };
};

export const storeItem = (db, integrationType) => auction => {
  const data = { ...auction };
  return db.put(auction.id, data).catch(err => {
    console.log(
      `ERROR: ${integrationType}'ing the auction into database has failed:`,
      err
    );
  });
};

export const writeToFile = async (auctionId, html) => {
  const pathToFile = settings.integration.paths.output + `/${auctionId}.html`;
  const _ = await fs.writeFile(pathToFile, html, "utf8");
  return pathToFile;
};

export const deleteFile = pathToFile => fs.unlink(pathToFile);

export const saveWordpressPost = async (integrationType, auction, postId) => {
  const html = buildHTML(auction);
  const pathToFile = await writeToFile(auction.id, html);
  const wordpressPost = { ...auction, postId };
  const command = buildCommand(wordpressPost, integrationType, pathToFile);
  try {
    const processOutput = await exec(command);
    const stdout = processOutput.stdout.toString();
    console.log(stdout);
    const postIdMatch = stdout.match(/\d+/g);
    auction.postId = postIdMatch.map(Number)[0];
    console.log(
      `LOG: ${integrationType}d a post with Wordpress id:`,
      auction.postId,
      "for auction with id:",
      auction.id
    );
    await deleteFile(pathToFile);
    return auction;
  } catch (err) {
    console.log(err);
    return err;
  }
};
