import { settings } from "../../../settings";
import { AuctionStatus, AuctionPriceRange } from "../model";
const moment = require("moment");
const $ = require("cheerio");
const prettier = require("prettier");

export const REQUIREMENTS_SECTION_TITLE = "WYMAGANIA I SPECYFIKACJA";
export const COMMENTS_SECTION_TITLE = "KOMUNIKATY";
export const OBJECTIVE_SECTION_TITLE = "PRZEDMIOT ZAMÓWIENIA";

export function getTitle(content) {
  const TITLE_SELECTOR = "h3 > span";
  const result = $(TITLE_SELECTOR, content);
  const unescaped = result.text();
  return unescaped.replace(/[\""]/g, '\\"').trim();
}

export function getDatePosted(content) {
  const DATE_POSTED_SELECTOR =
    ".table-dates > tbody > tr:first-child > td:last-child";
  const dateString = $(DATE_POSTED_SELECTOR, content).text();
  return moment(dateString, settings.integration.date_format).format(
    settings.wordpress.date_format
  );
}

export function getDates(content) {
  const DATE_ROWS_SELECTOR = ".table-dates";
  const dates = $(DATE_ROWS_SELECTOR, content).html();
  return `
<h3>Terminy</h3>
${dates}
`;
}

export function getRequirements(content) {
  const requirementsSectionTitle = getHeaderElement(
    REQUIREMENTS_SECTION_TITLE,
    content
  );
  if (requirementsSectionTitle) {
    const parentRow = $(requirementsSectionTitle).closest(".row");
    const uglyRequirementsSection = $(parentRow)
      .siblings("#requirements")
      .html();
    const prettified = prettier.format(uglyRequirementsSection, {
      parser: "html",
      printWidth: 500,
    });
    return `
<h3>${$(requirementsSectionTitle).html()}</h3>
<br />
${prettified}
`;
  } else {
    return null;
  }
}

export function getAttachments(content) {
  const ATTACHMENTS_TITLE_SELECTOR = "#requirements";
  return $(ATTACHMENTS_TITLE_SELECTOR, content)
    .next(".row")
    .next(".row")
    .html();
}

export function getObjectives(content) {
  const objectiveSectionTitle = getHeaderElement(
    OBJECTIVE_SECTION_TITLE,
    content
  );
  if (objectiveSectionTitle) {
    const parentRow = $(objectiveSectionTitle).closest(".row");
    const objectivesTableSection = $(parentRow)
      .siblings(".offer-form")
      .html();
    const objectivesTable = $(
      ".table-responsive",
      objectivesTableSection
    ).first();
    const columns = $(objectivesTable)
      .find("td")
      .get().length;
    for (var i = 4; i < columns; i++) {
      $(objectivesTable)
        .find("th")
        .eq(4)
        .remove();
      $(objectivesTable)
        .find("td")
        .eq(4)
        .remove();
    }
    return `
<h3>${$(objectiveSectionTitle).html()}</h3>
${objectivesTable.html()}
`;
  } else {
    return null;
  }
}

export function getComments(content) {
  const commentsSectionTitle = getHeaderElement(
    COMMENTS_SECTION_TITLE,
    content
  );
  if (commentsSectionTitle) {
    const parentRow = $(commentsSectionTitle).closest(".row");
    const uglyComments = $(parentRow)
      .siblings("#comments_info")
      .html();
    const prettified = prettier
      .format(uglyComments, {
        parser: "html",
        printWidth: 500,
      })
      .replace(/style="([^"]*)"/g, "")
      .replace(/strong/g, "b");
    return `
<h3>${$(commentsSectionTitle).html()}</h3>
${prettified}
`;
  } else {
    return null;
  }
}

export function getPriceRangeCategory(title) {
  if (title.includes(AuctionPriceRange.UP_TO_30K.keyword)) {
    return AuctionPriceRange.UP_TO_30K.wordpressCategory;
  } else if (title.includes(AuctionPriceRange.FROM_30K_TO_221K.keyword)) {
    return AuctionPriceRange.FROM_30K_TO_221K.wordpressCategory;
  } else if (title.includes(AuctionPriceRange.OVER_221K.keyword)) {
    return AuctionPriceRange.OVER_221K.wordpressCategory;
  } else {
    return undefined;
  }
}

export function getLastNexusModificationDate(content) {
  const NOTICE_DATE_COLUMN_INDEX = 1;
  const lastNoticeDateSelector = `.table-responsive > table tr:nth-child(1) td:nth-child(${NOTICE_DATE_COLUMN_INDEX}) .highlight-text`;
  const lastNoticeDateText = $(lastNoticeDateSelector, content).html();
  return moment(lastNoticeDateText).toDate();
}

export const getNextPageUrl = content => {
  const NEXT_PAGE_SELECTOR = ".pagination li a";
  const NEXT_PAGE_SIGN = ">";
  const paginationLinks = $(NEXT_PAGE_SELECTOR, content).get();

  const nextPageSignIndex = paginationLinks.findIndex(link => {
    const test = $(link).text();
    return test === NEXT_PAGE_SIGN;
  });

  if (nextPageSignIndex !== -1) {
    const nextPageRelativeUrl = paginationLinks[nextPageSignIndex];
    const relativeUrl = $(nextPageRelativeUrl)[0].attribs["href"];
    return `${settings.integration.urls.auction_list}${relativeUrl}`;
  }
  return false;
};

const getHeaderElement = (headerText, content) => {
  const OBJECTIVES_TITLE_SELECTOR = ".row h3";
  let sectionTitles = $(OBJECTIVES_TITLE_SELECTOR, content).get();
  return sectionTitles.find(
    title =>
      $(title)
        .text()
        .toUpperCase()
        .trim() === headerText
  );
};
