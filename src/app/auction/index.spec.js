import { expect } from "chai";
import moment from "moment";
import {
  fakeAuctionOpen,
  fakeAuctions1stPage,
  fakeAuctions2ndPage,
  fakeAuctionsClosedOnly,
} from "../../fixtures";
import { fetchStub } from "../../test-setup.spec";
import { fetchAuctionContent, getIds } from "./index";

describe("Auction module", () => {
  it("should get ids from all auctions on every page", async () => {
    // GIVEN
    const ANY_URL = "";
    fetchStub.onCall(0).resolves(fakeAuctions1stPage);
    fetchStub.onCall(1).resolves(fakeAuctions2ndPage);
    // WHEN
    const result = await getIds(ANY_URL);
    // THEN
    expect(fetchStub.callCount).to.eql(2);
    expect(Array.isArray(result)).to.eql(true);
    expect(result.length).to.eql(3);
    expect(result).to.eql(["242215", "240711", "149171"]);
  });

  it("should skip empty ongoing auctions section", async () => {
    // GIVEN
    const ANY_URL = "";
    fetchStub.resolves(fakeAuctionsClosedOnly);
    // WHEN
    const result = await getIds(ANY_URL);
    // THEN
    expect(Array.isArray(result)).to.eql(true);
    result.forEach(val => {
      expect(parseInt(val) !== NaN);
    });
    expect(result.length).to.eql(6);
  });

  it("should fetch the auction data", async () => {
    // GIVEN
    const ANY_AUCTION_ID = 0;
    fetchStub.resolves(fakeAuctionOpen);
    // WHEN
    const result = await fetchAuctionContent(ANY_AUCTION_ID);
    // THEN
    const keysToCheck = [
      result.id,
      result.title,
      result.posted,
      result.dates,
      result.requirements,
      result.attachments,
      result.comments,
      result.objectives,
    ];
    keysToCheck.forEach(prop => {
      expect(prop).not.to.be.undefined;
    });
    expect(result.id).to.eql(ANY_AUCTION_ID);
    expect(result.title).to.contain("Dostawa produktów leczniczych różnych");
    expect(
      moment(result.posted).isBetween(
        moment("2019-07-22"),
        moment("2019-07-23")
      )
    );
  });
});
