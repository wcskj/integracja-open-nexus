import { settings } from "../../settings";
const level = require("level");

export const createDb = () => {
  const db = level(settings.integration.paths.db, {
    valueEncoding: "json",
  });
  return db;
};
