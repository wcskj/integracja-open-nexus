import { discover } from "wp-cli";
import { settings } from "../../settings";

export const getWordpressPostById = postId =>
  new Promise((resolve, reject) => {
    discover({ path: settings.wordpress.location }, WordpressInstance => {
      WordpressInstance.post.get(postId, (err, comment) => {
        if (err) reject(err);
        resolve(comment);
      });
    });
  });
