import { settings } from "../../settings";
import { IntegrationType } from "./integration/type";

export const buildUrl = id => `${settings.integration.urls.auction}/${id}`;

export const buildCommand = (auction, integrationType, pathToFile) => {
  return `wp post ${integrationType} \
${
  integrationType === IntegrationType.UPDATE && !!auction.postId
    ? auction.postId + " "
    : ""
}\
--post_title="${auction.title}" \
--post_date="${auction.posted}" \
--post_author="${settings.wordpress.auctions_author_id}" \
--post_status="publish" \
--post_category=${getPostCategories(auction)} \
--tags_input="${settings.wordpress.auction_tags}" \
--path="${settings.wordpress.location}" \
--allow-root \
${pathToFile}`;
};

const getPostCategories = auction => {
  return [settings.wordpress.auction_category, auction.priceRange]
    .filter(category => !!category)
    .join(",");
};

export const buildHTML = auction => {
  const exisitingAuctionProperties = [
    auction.dates,
    auction.requirements,
    auction.attachments,
    auction.comments,
    auction.objectives,
  ].filter(auctionProp => !!auctionProp);
  const htmlSections = exisitingAuctionProperties
    .map(prop => `<section>${prop}</section>`)
    .join("<p></p>");
  return htmlSections;
};
