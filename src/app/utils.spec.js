import { expect } from "chai";
import { buildCommand } from "./utils";
import { IntegrationType } from "./integration/type";

describe("Utils module", () => {
  describe("buildCommand", () => {
    it("should build a proper command for CREATE", () => {
      const result = buildCommand({}, IntegrationType.CREATE, "fakePath");
      expect(result.startsWith("wp post create --post_title")).to.be.true;
    });

    it("should build a proper command for UPDATE", () => {
      const result = buildCommand(
        { postId: 123 },
        IntegrationType.UPDATE,
        "fakePath"
      );
      expect(result.startsWith("wp post update 123 --post_title")).to.be.true;
    });
  });
});
