export const AuctionStatus = {
  OPEN: 0,
  CLOSED: 1
};

export const AuctionPriceRange = {
  // in Euros
  UP_TO_30K: {
    keyword: "NPU",
    wordpressCategory: "do-30tys-euro"
  },
  FROM_30K_TO_221K: {
    keyword: "BZP",
    wordpressCategory: "30tys-221tys-euro"
  },
  OVER_221K: {
    keyword: "DUUE",
    wordpressCategory: "powyzej-221tys-euro"
  }
};
