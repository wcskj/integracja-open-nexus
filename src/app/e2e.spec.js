import { expect } from "chai";
import { fake } from "sinon";
import * as util from "util";
import {
  fakeAuctionOpen,
  fakeAuctionOpenModifiedAttachment,
  fakeAuctionOpenModifiedComment,
} from "../fixtures";
import { fetchStub, sandbox } from "../test-setup.spec";
import * as auction from "./auction/index";
import * as db from "./db";
import * as integration from "./integration/index";
import { IntegrationType } from "./integration/type";

describe("OpenNexus Integration e2e", () => {
  const FAKE_AUCTIONS_URL = "fakeAuctionsUrl";
  const FIRST_AUCTION_ID = "firstID";
  const FAKE_WORDPRESS_ID = "wordpressPostID";
  let storeItemStub;
  let getIdsStub;
  let dbStub;
  let writeToFileStub;
  let execStub;
  let deleteFileStub;

  beforeEach(() => {
    dbStub = sandbox.stub(db, "createDb");
    dbStub.returns({
      get: key => Promise.resolve(null),
      put: _ => Promise.resolve(true),
    });
    getIdsStub = sandbox.stub(auction, "getIds");
    getIdsStub.resolves([FIRST_AUCTION_ID]);
    storeItemStub = sandbox.stub(auction, "storeItem");
    writeToFileStub = sandbox.stub(auction, "writeToFile").resolves(true);
    execStub = sandbox
      .stub(util, "promisify")
      .returns(_ => Promise.resolve(true));
    deleteFileStub = sandbox.stub(auction, "deleteFile").resolves(true);
  });

  it("should create the new auction", async () => {
    // GIVEN
    fetchStub.resolves(fakeAuctionOpen);
    // WHEN
    const result = await integration.runIntegration(FAKE_AUCTIONS_URL);
    // THEN
    expect(result).to.eql(IntegrationType.CREATE);
  });

  it("should do nothing when the auction found in db is the same", async () => {
    // GIVEN
    fetchStub.resolves(fakeAuctionOpen);
    const alreadyStored = await auction.fetchAuctionContent(FIRST_AUCTION_ID);
    dbStub.returns({
      get: fake.resolves({
        ...alreadyStored,
        postId: FAKE_WORDPRESS_ID,
      }),
    });
    const result = await integration.runIntegration(FAKE_AUCTIONS_URL);
    expect(result).to.eql(IntegrationType.STALE);
  });

  it("should update the post when attachments have been modified", async () => {
    // GIVEN
    fetchStub.onCall(0).resolves(fakeAuctionOpen);
    fetchStub.onCall(1).resolves(fakeAuctionOpenModifiedAttachment);
    const alreadyStored = await auction.fetchAuctionContent(FIRST_AUCTION_ID);
    dbStub.returns({
      get: fake.resolves({
        ...alreadyStored,
        postId: FAKE_WORDPRESS_ID,
      }),
    });
    const result = await integration.runIntegration(FAKE_AUCTIONS_URL);
    expect(result).to.eql(IntegrationType.UPDATE);
  });

  it("should update the post when comments have been modified", async () => {
    // GIVEN
    fetchStub.onCall(0).resolves(fakeAuctionOpen);
    fetchStub.onCall(1).resolves(fakeAuctionOpenModifiedComment);
    const alreadyStored = await auction.fetchAuctionContent(FIRST_AUCTION_ID);
    dbStub.returns({
      get: fake.resolves({
        ...alreadyStored,
        postId: FAKE_WORDPRESS_ID,
      }),
    });
    // WHEN
    const result = await integration.runIntegration(FAKE_AUCTIONS_URL);
    // THEN
    expect(result).to.eql(IntegrationType.UPDATE);
  });
});
