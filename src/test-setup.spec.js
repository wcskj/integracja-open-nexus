import { use } from "chai";
import { createSandbox } from "sinon";
import sinonChai from "sinon-chai";
const fetch = require("request-promise");

let sandbox, fetchStub;

before(() => {
  use(sinonChai);
  sandbox = createSandbox();
});

beforeEach(() => {
  fetchStub = sandbox.stub(fetch, "Request");
});

afterEach(() => {
  sandbox.restore();
});

export { sandbox, fetchStub };
