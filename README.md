# integracja-open-nexus

## Wymagania

1. NodeJS 8+
2. Wordpress CLI
3. Istniejąca instalacja Wordpress

## Uruchomienie

Nominalnie aplikacja jest skierowana do uruchomienia wewnątrz kontenera opisanego przez `Dockerfile`, który wypełnia **# Wymagania**. Niemniej uruchomienie lokalne jest również możliwe:

```bash
npm install
npm start
```
