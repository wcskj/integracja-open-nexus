export const settings = {
  integration: {
    urls: {
      auction_list: "https://platformazakupowa.pl/wcskj/aukcje",
      auction: "https://platformazakupowa.pl/transakcja"
    },
    paths: {
      db: "out/auction-db",
      output: "out"
    },
    date_format: "DD-MM-YYYY HH:mm:ss"
  },
  wordpress: {
    location: "/var/www/html",
    auction_category: "zamowienia-publiczne",
    auction_tags: ["open-nexus"],
    auctions_author_id: "5", // resolves to 'opennexus' user in Wordpress
    date_format: "YYYY-MM-DD HH:mm:ss"
  }
};
