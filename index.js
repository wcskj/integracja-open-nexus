import { runIntegration } from "./src/app/integration";
import { settings } from "./settings";

// MAIN
runIntegration(settings.integration.urls.auction_list).catch(err => {
  console.log("Something went wrong.");
  throw new Error(err);
});
